"""How to use Fortran to speed up calculus in Python

illustrated on computing the curl of (u, v)

"""
import build
from numba import jit
import numpy as np
import time
from ctypes import POINTER, c_double, c_int32, c_int8, byref, cdll
import os


MAP_TYPES = {
    "float64": c_double,
    "int8": c_int8,
    "int32": c_int32,
}


build.build()
library = "libfortran.so"
lib = cdll.LoadLibrary(f"./{library}")
curl_fortran = lib.curl_


def ptr(array):
    """ return `array` pointer"""
    return array.ctypes.data_as(POINTER(MAP_TYPES[str(array.dtype)]))


def curl_numpy(u, v, vor):
    vor[:, 1:-1] = np.diff(v, axis=1)
    vor[1:-1, :] -= np.diff(u, axis=0)


@jit
def curl_numba(u, v, vor, shape):
    nx, ny, nh = shape
    for j in range(nh, ny+nh+1):
        for i in range(nh, nx+nh+1):
            vor[j, i] = v[j, i]-v[j, i-1]-u[j, i]+u[j-1, i]


def time_func(func, args):
    nt = 1_000
    tic = time.time()
    for _ in range(nt):
        func(*args)
    toc = time.time()
    return (toc-tic)/nt


def print_perfs(fct_to_time, noperations):

    print(f" "*9+"time per call   speed")
    print(f" "*9+"   (1e-6 s)   (GFlops/s)")
    tab = " "*9
    for name in fct_to_time:
        elapsed = time_func(*fct_to_time[name])
        gflops = noperations/elapsed/1e9
        print(f"{name:10} {elapsed*1e6:.1f} {tab} {gflops:.2f}")


def disassemble(arg, type=None):
    if type == "numba":
        code = arg.inspect_asm()
        with open("code_numba.asm", "w") as fid:
            fid.write(list(code.values())[0])
    elif type == "Fortran":
        command = f"objdump -S {library} > code_fortran.asm"
        os.system(command)
    else:
        raise ValueError
    print(f"generate assembly code for {type}")


if __name__ == "__main__":

    ny, nx = 200, 200
    nh = 2
    nyh = ny+2*nh
    nxh = nx+2*nh

    u = np.zeros((nyh, nxh+1))
    v = np.zeros((nyh+1, nxh))
    vor = np.zeros((nyh+1, nxh+1))
    shape = np.asarray([nx, ny, nh], dtype="i4")

    fortran_args = (ptr(u), ptr(v), ptr(vor), ptr(shape))

    # non trivial velocity field (shear)
    for j in range(ny):
        u[j+nh, nh:-nh] = j

    # test that the curl is -1 everywhere
    vor.flat = 0
    curl_numba(u, v, vor, shape)
    assert np.allclose(vor[nh+1:-nh-1, nh+1:-nh], -1.)

    vor.flat = 0
    curl_numpy(u, v, vor)
    assert np.allclose(vor[nh+1:-nh-1, nh+1:-nh], -1.)

    vor.flat = 0
    curl_fortran(*fortran_args)
    assert np.allclose(vor[nh+1:-nh-1, nh+1:-nh], -1.)

    functions = {
        "numpy": (curl_numpy, (u, v, vor)),
        "numba": (curl_numba, (u, v, vor, shape)),
        "fortran": (curl_fortran, fortran_args),
    }

    nope = (nx+1)*(ny+1)*3

    # measure the performances for all three methods
    print_perfs(functions, nope)

    disassemble(curl_numba, type="numba")
    disassemble(library, type="Fortran")
