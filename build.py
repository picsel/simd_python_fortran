"""
Why use Makefiles when Python does it so easily?

Compile the Fortran subroutines and generate the dynamic libraries

"""
import os


def compile_fortran(compiler, flags, srcs, library):
    prefix, suffix = srcs.split(".")

    srcs_cpped = f"{prefix}_.{suffix}"
    command = f"cpp -P {srcs} > {srcs_cpped}"
    os.system(command)

    command = f"{compiler} {flags} {srcs_cpped} -fPIC -shared -o lib{library}.so"
    print(command)
    os.system(command)

    command = f"rm -f {srcs_cpped}"
    os.system(command)


def get_compiler():
    nodename = os.uname().nodename
    if "irene" in nodename:
        compiler = "ifort"
        flags = "-r8 -O3 -axCORE-AVX512"

    else:
        compiler = "gfortran"
        flags = "-freal-4-real-8 -Ofast -march=x86-64 -mtune=native -ffast-math  -ffree-line-length-none"

    return compiler, flags


def build():
    compiler, flags = get_compiler()

    modules = {
        "fortran": {
            "srcs": "fortranfunctions.f90",
            "library": "fortran"},
    }

    for mod, val in modules.items():
        compile_fortran(compiler, flags, val["srcs"], val["library"])


if __name__ == "__main__":

    build()
