# Tutorial on how to use Fortran to speed up a Python code

## Intro

  - fast, slow ? faster -> these are relative terms
  - measure against absolute speed -> GFlops

  - in the terminal `lscpu` to get the specs of your cpu
  - pick the name of your cpu, search on https://www.techpowerup.com/cpu-specs/
  for Guillaume's laptop https://www.techpowerup.com/cpu-specs/core-i7-6700t.c1857

  - the max GFlops/s is much larger the clock frequency(actually x16 times)

  - two reasons:
      - multicore + hyperthreading(x2)
      - SIMD: Single Instruction Multiple Data(aka vectorization)

  - SIMD can bring a x8 speedup!
  - how ? -> this tutorial

## General recipe to use Fortran

  - compile the code `myprog.f90` into a dynamic library

    gfortran -freal-4-real-8 -Ofast -march=x86-64 -mtune=native -ffast-math -ffree-line-length-none -fPIC -shared myprog.f90 -o libfortran.so

  - import the library within Python with `ctypes` cdll.LoadLibrary()

  - call the function with arrays as pointers and numbers with
    byref(c_type(value)
  see code

  - that's it


## Example on the curl

  - example on computing the curl of(u, v)

  1) using numpy np.diff
  2) using explicit loop + numba/jit(just in time compilation)
  3) using a Fortran implementation of the loop

  check the performances in GFlops/s

  incidentaly, the Fortran compilation is done within Python with the `build.py` module (own creation that could be improved). I use macros and a preprocessing phase

  look at the assembly code to see the SIMD instructions



## Conclusion & Perspectives

  - we can harness SIMD in Python
  - we have used Python instead of `make` to compile -> so cool!
  - we can even compile Fortran modules and run whole Fortran codes from Python!
