#define type_shape integer, dimension(3):: shape

#define nx shape(1)
#define ny shape(2)
#define nh shape(3)

#define xcenters 1-nh:nx+nh
#define xedges   1-nh:nx+nh+1
#define ycenters 1-nh:ny+nh
#define yedges   1-nh:ny+nh+1


subroutine curl(u, v, vor, shape)

  implicit none

  type_shape
  
  real, dimension(xedges  , ycenters):: u
  real, dimension(xcenters, yedges  ):: v
  real, dimension(xedges  , yedges  ):: vor

  integer:: i, j, k

  do j = 1, ny+1
     do i = 1, nx+1
        vor(i,j) = v(i,j)-v(i-1,j)-u(i,j)+u(i,j-1) 
     enddo
  enddo

end subroutine curl
